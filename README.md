# PROJECT ARGENTBANK

Bank app in React, fetching data from a micro API.

## Project dependencies :
+ React v17.0.1
+ react-router-dom v5.2.0
+ React Redux 7.1+
+ axios
+ Visual Studio Code (recommanded)

What you need to install and run the project :

+ Git to clone the repository
+ Yarn
+ Clone the project to your computer
git clone https://gitlab.com/saubletg/saubletgildas_13_21072021
+ Go to the project folder
cd GildasSaublet_13_21072021
+ Install the packages
yarn
+ Run the project (port 3000 by default)
yarn start
+ To get the backend API, fork this repo and follow the instructions
https://github.com/OpenClassrooms-Student-Center/Project-10-Bank-API