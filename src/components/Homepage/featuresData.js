export const featuresData = [
    {
      id: "feature-01",
      imgSrc: "icon-chat.png",
      imgAlt: "Chat Icon",
      title: "You are our #1 priority",
      description:
        "Need to talk to a representative? You can get in touch through our 24/7 chat or through a phone call in less than 5 minutes.",
    },
    {
      id: "feature-02",
      imgSrc: "icon-money.png",
      imgAlt: "Chat Icon",
      title: "More savings means higher rates",
      description:
        "The more you save with us, the higher your interest rate will be!",
    },
    {
      id: "feature-03",
      imgSrc: "icon-security.png",
      imgAlt: "Chat Icon",
      title: "Security you can trust",
      description:
        "We use top of the line encryption to make sure your data and money is always safe.",
    },
];